package com.sisWeb.sisWen.Repositories;

import com.sisWeb.sisWen.Entities.Book;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends CrudRepository<Book, Integer> {
        Book findByTitle(String title);
}
