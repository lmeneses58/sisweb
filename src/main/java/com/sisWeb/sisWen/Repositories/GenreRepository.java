package com.sisWeb.sisWen.Repositories;

import com.sisWeb.sisWen.Entities.Genre;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GenreRepository extends CrudRepository<Genre, Integer> {
}
