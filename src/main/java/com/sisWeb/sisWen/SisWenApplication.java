package com.sisWeb.sisWen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SisWenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SisWenApplication.class, args);
	}

}
