package com.sisWeb.sisWen.Controllers;

import com.sisWeb.sisWen.Entities.Book;
import com.sisWeb.sisWen.Entities.Genre;
import com.sisWeb.sisWen.Repositories.BookRepository;
import com.sisWeb.sisWen.Services.BookService;
import com.sisWeb.sisWen.Services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class BookController {

    private BookService bookService;
    private GenreService genreService;

    @Autowired
    public void setGenreService(GenreService genreService) {
        this.genreService = genreService;
    }

    @Autowired
    public void setBookService (BookService bookService){
        this.bookService=bookService;
    }

    @RequestMapping(value = "/newBook", method = RequestMethod.GET)
    public String newBook(Model model) {
        List<Genre> bookGenres =
                (List<Genre>) genreService.listAllGenres();
        model.addAttribute("book", new Book());
        model.addAttribute("bookGenres", bookGenres);
        return "newBook";
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    public String create(@ModelAttribute("book") Book book, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "newBook";
        }
        bookService.save(book);
        return "redirect:/";
    }

    @RequestMapping("/deleteBook/{id}")
    String deleteBook (@PathVariable Integer id) {
        bookService.delete(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/book/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model) {
        Book book = bookService.findById(id);
        model.addAttribute("book", book);
        List<Genre> bookGenres
                = (List<Genre>) genreService.listAllGenres();

        model.addAttribute("bookGenres", bookGenres);
        return "editBook";
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String index( Model model) {
        List<Book> books  = (List) bookService.listAllBooks();
        model.addAttribute("books", books);
        return "books";
    }

}

