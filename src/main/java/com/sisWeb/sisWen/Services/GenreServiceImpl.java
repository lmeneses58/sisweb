package com.sisWeb.sisWen.Services;

import com.sisWeb.sisWen.Entities.Genre;
import com.sisWeb.sisWen.Repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GenreServiceImpl implements GenreService {

    private GenreRepository genreRepository;

    @Autowired
    @Qualifier (value = "genreRepository")
    public void setGenreRepository(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public Iterable<Genre> listAllGenres() {
        return genreRepository.findAll();
    }
}
