package com.sisWeb.sisWen.Services;

import com.sisWeb.sisWen.Entities.Genre;

public interface GenreService {
    Iterable<Genre> listAllGenres();

}