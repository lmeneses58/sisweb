package com.sisWeb.sisWen.Services;

import com.sisWeb.sisWen.Entities.Book;

public interface BookService {
    Book findByTitle(String title);
    Iterable<Book> listAllBooks();
    void save(Book book);
    void delete( Integer id);
    Book findById(Integer id);
}
